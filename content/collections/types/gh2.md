---
id: 5cb3d338-5e88-49e3-8ed3-4ca5a93df34d
blueprint: type
title: GH2
configuration:
  -
    group: Afmetingen
    parameters:
      -
        parameter: Hoofdgebouw
        values:
          -
            val: '13,93 x 8,71 meter'
            price: 0.0
          -
            val: '14,87 x 8,71 meter'
            price: 21120.0
          -
            val: '13,93 x 9,66 meter'
            price: 35210.0
          -
            val: '14,87 x 9,66 meter'
            price: 56470.0
        type: parameters
        enabled: true
    type: group
    enabled: true
  -
    group: Indeling
    parameters:
      -
        parameter: 'Begane grond'
        values:
          -
            val: BH01
            price: 0.0
          -
            val: BH02
            price: 3895.0
          -
            val: BH03
            price: 8380.0
          -
            val: BH04
            price: 2565.0
          -
            val: BH05
            price: 7080.0
          -
            val: BH06
            price: -1145.0
          -
            val: BH07
            price: 0.0
        type: parameters
        enabled: true
      -
        parameter: 'Verdieping 1'
        values:
          -
            val: VH01
            price: 0.0
          -
            val: VH02
            price: 5045.0
          -
            val: VH03
            price: 1735.0
          -
            val: VH04
            price: -495.0
        type: parameters
        enabled: true
      -
        parameter: 'Verdieping 2'
        values:
          -
            val: V2H01
            price: 0.0
          -
            val: V2H02
            price: 2454.0
          -
            val: V2H03
            price: 9305.0
        type: parameters
        enabled: true
    type: group
    enabled: true
  -
    group: Opties
    parameters:
      -
        parameter: Steenkleur
        values:
          -
            val: Bruin
            price: 0.0
            img: screenshot-2022-11-04-at-11.45.07.png
            delta: brick_brown
          -
            val: Wit
            price: 0.0
            img: screenshot-2022-11-04-at-11.45.17.png
            delta: brick_white
        type: parameters
        enabled: true
      -
        parameter: Wandopening
        values:
          -
            val: Serre
            price: 25170.0
            img: 92-103-2015-mv.jpg
            delta: delta-serre
          -
            val: Deuren
            price: 4000.0
            img: 92-102-2015-mv.jpg
            delta: delta-deuren
        type: parameters
        enabled: true
      -
        parameter: Berging
        values:
          -
            val: 'Plat dak | 4,00 x 7,00 meter'
            price: 35460.0
            img: 92-201-2015-mv.jpg
          -
            val: 'Schildkap | 5,39 x 8,71 meter'
            price: 65000.0
            img: 92-204-2015-mv.jpg
          -
            val: 'Dubbele berging schildkap | 7,29 x 8,71 meter'
            price: 85130.0
            img: 92-205-2015-mv.jpg
        type: parameters
        enabled: false
      -
        parameter: 'Dakopening achter'
        values:
          -
            val: '3x dakraam'
            price: 4000.0
            img: 90-470r-kh3-2016-mv.jpg
            delta: delta-dakraam
          -
            val: '3x dakkapel'
            price: 10790.0
            img: 90-471r-kh3-2016-mv.jpg
            delta: delta-dakkapel
          -
            val: Geen
            price: 0.0
            delta: delta-dicht
        type: parameters
        enabled: true
    type: group
    enabled: true
updated_by: 6db32850-1089-4f66-aedf-c8556f20a2cc
updated_at: 1667558837
---

module.exports = {
  content: [
    './resources/**/*.antlers.html',
    './resources/**/*.blade.php',
    './resources/**/*.vue',
    './content/**/*.md'
  ],
  theme: {
    extend: {
      colors : {
        'primary' : '#0099cc'
      },
      fontFamily: {
        primary: 'poppins'
      }
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}

import Alpine from 'alpinejs'
 
window.Alpine = Alpine
 
Alpine.start()

const elem = document.getElementById('panzoom')
const panzoom = Panzoom(elem, {
  maxScale: 5
})